# mkpm packages

> official mkpm packages

## Packages

| Package                                                  | Version | Description                                | Author                             |
| -------------------------------------------------------- | ------- | ------------------------------------------ | ---------------------------------- |
| [gnu](https://gitlab.com/bitspur/mkpm/gnu.git)           | 0.1.0   | add gnu commands to makefiles              | Clay Risser <clayrisser@gmail.com> |
| [docker](https://gitlab.com/bitspur/mkpm/docker.git)     | 0.3.16  | build docker images using make             | Clay Risser <clayrisser@gmail.com> |
| [hello](https://gitlab.com/bitspur/mkpm/hello.git)       | 0.2.0   | mkpm hello world package                   | Clay Risser <clayrisser@gmail.com> |
| [patch](https://gitlab.com/bitspur/mkpm/patch.git)       | 0.0.5   | patch 3rd party projects                   | Clay Risser <clayrisser@gmail.com> |
| [yarn](https://gitlab.com/bitspur/mkpm/yarn.git)         | 0.4.0   | manage yarn projects                       | Clay Risser <clayrisser@gmail.com> |
| [deps](https://gitlab.com/bitspur/mkpm/deps.git)         | 0.0.5   | manage deps using make                     | Clay Risser <clayrisser@gmail.com> |
| [envcache](https://gitlab.com/bitspur/mkpm/envcache.git) | 0.3.1   | cache calculated envs for faster load time | Clay Risser <clayrisser@gmail.com> |
| [dotenv](https://gitlab.com/bitspur/mkpm/dotenv.git)     | 0.2.3   | dotenv support for makefiles               | Clay Risser <clayrisser@gmail.com> |
| [python](https://gitlab.com/bitspur/mkpm/python.git)     | 0.1.3   | manage python projects                     | Clay Risser <clayrisser@gmail.com> |
| [chain](https://gitlab.com/bitspur/mkpm/chain.git)       | 0.2.4   | chained actions for makefiles              | Clay Risser <clayrisser@gmail.com> |
| [pkg](https://gitlab.com/bitspur/mkpm/pkg.git)           | 1.0.1   | create packages using make                 | Clay Risser <clayrisser@gmail.com> |
| [prisma](https://gitlab.com/bitspur/mkpm/prisma.git)     | 0.1.3   | manage prisma using make                   | Clay Risser <clayrisser@gmail.com> |
